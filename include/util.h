#ifndef _FUP_UTIL_H
#define _FUP_UTIL_H

int load_file_content(struct options *opr, char *filepath);
int load_piped_content(struct options *opr);

#endif
