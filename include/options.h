#ifndef _FUP_OPTIONS_H
#define _FUP_OPTIONS_H

#include <stddef.h>

enum paste_operation {
	NORMAL = 0,
	NAMED = 1,
	LIST = 2,
	LIST_ALL = 3,
	DELETE = 4,
};

/*
 * Max size of a paste name is 20 characters, but UUID can take up to
 * 36 characters.
 */
struct options {
	char paste_name[40];
	enum paste_operation operation_code;
	unsigned char *paste_content;
	size_t paste_size;
};

void init_options(struct options *opt);
void destroy_options(struct options *opt);

#endif
