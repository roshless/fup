#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "config.h"
#include "options.h"
#include "post.h"
#include "util.h"

void print_usage(void)
{
	fprintf(stderr,
		"Usage: fup [-n paste_name|-l|-L|-d paste_id] [file|-]\n"
		"Pass - as a file to use stdin.\n");
}

int main(int argc, char **argv)
{
	struct settings cfg;
	struct options opr;
	init_options(&opr);

	int c;
	while ((c = getopt(argc, argv, "hlLn:d:")) != -1) {
		switch (c) {
		case 'n':
			opr.operation_code = NAMED;
			strcpy(opr.paste_name, optarg);
			break;
		case 'l':
			opr.operation_code = LIST;
			break;
		case 'L':
			opr.operation_code = LIST_ALL;
			break;
		case 'd':
			opr.operation_code = DELETE;
			strcpy(opr.paste_name, optarg);
			break;
		case 'h':
			print_usage();
			destroy_options(&opr);
			return EXIT_SUCCESS;
		}
	}
	if (load_config(&cfg) != 0) {
		fprintf(stderr,
			"Couldn't load config "
			"($HOME/.config/fup/fup.ini)\n");
		return EXIT_FAILURE;
	}

	if ((opr.operation_code == NAMED || opr.operation_code == NORMAL) &&
		argc - optind != 1) {
		destroy_config(&cfg);
		destroy_options(&opr);
		print_usage();
		return EXIT_FAILURE;
	}

	int ret = 0;
	if (!isatty(fileno(stdin))) {
		ret = load_piped_content(&opr);
	} else if (opr.operation_code == NAMED ||
		   opr.operation_code == NORMAL) {
		ret = load_file_content(&opr, argv[optind]);
	}

	if (ret != 0) {
		destroy_config(&cfg);
		destroy_options(&opr);
		fprintf(stderr, "Reading file failed\n");
		return EXIT_FAILURE;
	}

	char *output;
	switch (opr.operation_code) {
	case NORMAL:
		output = post_paste(&opr, &cfg);
		break;
	case NAMED:
		output = post_named_paste(&opr, &cfg);
		break;
	case LIST:
		output = list_user_pastes(&opr, &cfg);
		break;
	case LIST_ALL:
		output = list_anonymous_pastes(&opr, &cfg);
		break;
	case DELETE:
		output = delete_paste(&opr, &cfg);
		break;
	}

	if (output != NULL) {
		printf("%s", output);
	}

	free(output);
	destroy_config(&cfg);
	destroy_options(&opr);
	return EXIT_SUCCESS;
}
