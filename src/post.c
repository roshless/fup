#include <curl/curl.h>
#include <string.h>

#include "post.h"

static size_t writer_output(void *data, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	struct memory *mem = (struct memory *)userp;

	char *ptr = realloc(mem->response, mem->size + realsize + 1);
	if (ptr == NULL)
		return 0;

	mem->response = ptr;
	memcpy(&(mem->response[mem->size]), data, realsize);
	mem->size += realsize;
	mem->response[mem->size] = 0;

	return realsize;
}

static struct curl_slist *make_headers(char *username_input,
	char *password_input)
{
	struct curl_slist *headers;
	headers = NULL;

	if (!username_input || !password_input)
		return NULL;
	char username_text[] = "username:";
	char *username = NULL;
	char password_text[] = "password:";
	char *password = NULL;

	username = malloc(strlen(username_input) + strlen(username_text) + 1);
	strcpy(username, username_text);
	strcat(username, username_input);

	password = malloc(strlen(password_input) + strlen(password_text) + 1);
	strcpy(password, password_text);
	strcat(password, password_input);

	headers = curl_slist_append(headers, username);
	headers = curl_slist_append(headers, password);

	free(username);
	free(password);
	return headers;
}

char *post_paste(struct options *opr, struct settings *set)
{
	CURL *curl_handle;
	CURLcode res;

	struct curl_slist *headers = make_headers(set->username, set->password);

	struct memory chunk;
	chunk.response = malloc(1);
	chunk.size = 0;

	if (set->server == NULL) {
		free(chunk.response);
		printf("Set server value in config\n");
		return NULL;
	}

	char url_part[] = "/api/";
	char *url = malloc(strlen(set->server) + strlen(url_part) + 1);
	strcpy(url, set->server);
	strcat(url, url_part);

	curl_handle = curl_easy_init();
	if (curl_handle) {
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEFUNCTION, writer_output);
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

		curl_easy_setopt(curl_handle, CURLOPT_URL, url);
		curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

		curl_easy_setopt(curl_handle, CURLOPT_POST, 1l);
		curl_easy_setopt(
			curl_handle, CURLOPT_POSTFIELDSIZE, opr->paste_size);
		curl_easy_setopt(
			curl_handle, CURLOPT_POSTFIELDS, opr->paste_content);
		curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "fup/1.0");

		res = curl_easy_perform(curl_handle);
		curl_easy_cleanup(curl_handle);
	}
	curl_slist_free_all(headers);
	free(url);

	if (res != CURLE_OK) {
		fprintf(stderr,
			"connection failed: %s\n",
			curl_easy_strerror(res));
		free(chunk.response);
		return NULL;
	}

	char *output = malloc(strlen(chunk.response) + 1);
	strcpy(output, chunk.response);
	free(chunk.response);

	return output;
}

char *post_named_paste(struct options *opr, struct settings *set)
{
	CURL *curl_handle;
	CURLcode res;

	struct curl_slist *headers = make_headers(set->username, set->password);

	struct memory chunk;
	chunk.response = malloc(1);
	chunk.size = 0;

	if (set->server == NULL) {
		free(chunk.response);
		printf("Set server value in config\n");
		return NULL;
	} else if (set->password == NULL || set->username == NULL) {
		free(chunk.response);
		printf("Only registered users are allowed to post named "
		       "pastes\n");
		return NULL;
	}

	char url_part[] = "/api/named/?id=";
	char *url = malloc(strlen(set->server) + strlen(url_part) + 1 +
			   strlen(opr->paste_name));
	strcpy(url, set->server);
	strcat(url, url_part);
	strcat(url, opr->paste_name);

	curl_handle = curl_easy_init();
	if (curl_handle) {
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEFUNCTION, writer_output);
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

		curl_easy_setopt(curl_handle, CURLOPT_URL, url);
		curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

		curl_easy_setopt(curl_handle, CURLOPT_POST, 1l);
		curl_easy_setopt(
			curl_handle, CURLOPT_POSTFIELDSIZE, opr->paste_size);
		curl_easy_setopt(
			curl_handle, CURLOPT_POSTFIELDS, opr->paste_content);
		curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "fup/1.0");

		res = curl_easy_perform(curl_handle);
		curl_easy_cleanup(curl_handle);
	}
	curl_slist_free_all(headers);
	free(url);

	if (res != CURLE_OK) {
		fprintf(stderr,
			"connection failed: %s\n",
			curl_easy_strerror(res));
		free(chunk.response);
		return NULL;
	}

	char *output = malloc(strlen(chunk.response) + 1);
	strcpy(output, chunk.response);
	free(chunk.response);

	return output;
}

char *delete_paste(struct options *opr, struct settings *set)
{
	CURL *curl_handle;
	CURLcode res;

	struct curl_slist *headers = make_headers(set->username, set->password);

	struct memory chunk;
	chunk.response = malloc(1);
	chunk.size = 0;

	if (set->server == NULL) {
		free(chunk.response);
		printf("Set server value in config\n");
		return NULL;
	} else if (set->password == NULL || set->username == NULL) {
		free(chunk.response);
		printf("Only registered users are allowed to delete pastes\n");
		return NULL;
	}

	char url_part[] = "/api/?id=";
	char *url = malloc(strlen(set->server) + strlen(url_part) + 1 +
			   strlen(opr->paste_name));
	strcpy(url, set->server);
	strcat(url, url_part);
	strcat(url, opr->paste_name);

	curl_handle = curl_easy_init();
	if (curl_handle) {
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEFUNCTION, writer_output);
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

		curl_easy_setopt(curl_handle, CURLOPT_URL, url);
		curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

		curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "fup/1.0");
		curl_easy_setopt(curl_handle, CURLOPT_CUSTOMREQUEST, "DELETE");

		res = curl_easy_perform(curl_handle);
		curl_easy_cleanup(curl_handle);
	}
	curl_slist_free_all(headers);
	free(url);

	if (res != CURLE_OK) {
		fprintf(stderr,
			"connection failed: %s\n",
			curl_easy_strerror(res));
		free(chunk.response);
		return NULL;
	}

	char *output = malloc(strlen(chunk.response) + 1);
	strcpy(output, chunk.response);
	free(chunk.response);

	return output;
}

char *list_user_pastes(struct options *opr, struct settings *set)
{
	CURL *curl_handle;
	CURLcode res;

	struct curl_slist *headers = make_headers(set->username, set->password);

	struct memory chunk;
	chunk.response = malloc(1);
	chunk.size = 0;

	if (set->server == NULL) {
		free(chunk.response);
		printf("Set server value in config\n");
		return NULL;
	} else if (set->password == NULL || set->username == NULL) {
		free(chunk.response);
		printf("Only registered users are allowed to list pastes\n");
		return NULL;
	}

	char url_part[] = "/api/";
	char *url = malloc(strlen(set->server) + strlen(url_part) + 1);
	strcpy(url, set->server);
	strcat(url, url_part);

	curl_handle = curl_easy_init();
	if (curl_handle) {
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEFUNCTION, writer_output);
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

		curl_easy_setopt(curl_handle, CURLOPT_HTTPGET, 1l);
		curl_easy_setopt(curl_handle, CURLOPT_URL, url);
		curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

		curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "fup/1.0");

		res = curl_easy_perform(curl_handle);
		curl_easy_cleanup(curl_handle);
	}
	curl_slist_free_all(headers);
	free(url);

	if (res != CURLE_OK) {
		fprintf(stderr,
			"connection failed: %s\n",
			curl_easy_strerror(res));
		free(chunk.response);
		return NULL;
	}

	char *output = malloc(strlen(chunk.response) + 1);
	strcpy(output, chunk.response);
	free(chunk.response);

	return output;
}

char *list_anonymous_pastes(struct options *opr, struct settings *set)
{
	CURL *curl_handle;
	CURLcode res;

	struct curl_slist *headers = make_headers(set->username, set->password);

	struct memory chunk;
	chunk.response = malloc(1);
	chunk.size = 0;

	if (set->server == NULL) {
		free(chunk.response);
		printf("Set server value in config\n");
		return NULL;
	} else if (set->password == NULL || set->username == NULL) {
		free(chunk.response);
		printf("Only registered users are allowed to list pastes\n");
		return NULL;
	}

	char url_part[] = "/api/anon/";
	char *url = malloc(strlen(set->server) + strlen(url_part) + 1);
	strcpy(url, set->server);
	strcat(url, url_part);

	curl_handle = curl_easy_init();
	if (curl_handle) {
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEFUNCTION, writer_output);
		curl_easy_setopt(
			curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

		curl_easy_setopt(curl_handle, CURLOPT_HTTPGET, 1l);
		curl_easy_setopt(curl_handle, CURLOPT_URL, url);
		curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

		curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "fup/1.0");

		res = curl_easy_perform(curl_handle);
		curl_easy_cleanup(curl_handle);
	}
	curl_slist_free_all(headers);
	free(url);

	if (res != CURLE_OK) {
		fprintf(stderr,
			"connection failed: %s\n",
			curl_easy_strerror(res));
		free(chunk.response);
		return NULL;
	}

	char *output = malloc(strlen(chunk.response) + 1);
	strcpy(output, chunk.response);
	free(chunk.response);

	return output;
}
