#define _XOPEN_SOURCE 700

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "options.h"

int load_file_content(struct options *opr, char *filepath)
{
	char full_path[PATH_MAX];
	if (realpath(filepath, full_path) == NULL)
		return 1;

	FILE *fp = fopen(full_path, "rb");
	if (fp == NULL) {
		fprintf(stderr, "File not found!\n");
		return 1;
	}

	int ret = fseek(fp, 0L, SEEK_END);
	if (ret != 0)
		goto err;

	ret = ftell(fp);
	if (ret == -1)
		goto err;
	opr->paste_size = ret;

	opr->paste_content = malloc(opr->paste_size + 1);

	if (opr->paste_content == NULL)
		goto err;

	if (fseek(fp, 0L, SEEK_SET) != 0) {
		goto err_free;
	}

	fread(opr->paste_content, 1, opr->paste_size, fp);
	if (ferror(fp) != 0) {
		fprintf(stderr, "Error reading file\n");
		goto err_free;
	}

	fclose(fp);
	return 0;

err:
	fclose(fp);
	return 1;

err_free:
	free(opr->paste_content);
	goto err;
}

int load_piped_content(struct options *opr)
{
	char buffer[32];
	size_t size, bytes_read = 0;

	while ((bytes_read = read(fileno(stdin), buffer, sizeof(buffer))) > 0) {
		int oldsize = size;
		unsigned char *temp =
			realloc(opr->paste_content, size += bytes_read);
		if (temp == NULL) {
			printf("Out of memory!\n");
			exit(1);
		}
		opr->paste_content = temp;
		int buffer_i = 0;
		for (unsigned int i = oldsize; i < size; i++) {
			opr->paste_content[i] = buffer[buffer_i++];
		}
	}
	opr->paste_size = size;
	return 0;
}
