#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "config.h"

int load_config(struct settings *set)
{
	set->server = NULL;
	set->username = NULL;
	set->password = NULL;

	char *config_path = getenv("HOME");

	if (config_path == NULL) {
		struct passwd *pwd = getpwuid(getuid());
		if (pwd)
			config_path = pwd->pw_dir;
	}

	if (config_path == NULL)
		return 1;

	strcat(config_path, "/.config/fup/fup.ini");

	FILE *file;
	char buffer[255]; /* max line width */

	file = fopen(config_path, "r");

	if (file == NULL)
		return 1;

	char *config_values[] = {"server=", "username=", "password="};
	int config_values_sizes[] = {
		strlen(config_values[0]),
		strlen(config_values[1]),
		strlen(config_values[2]),
	};

	while (fgets(buffer, sizeof(buffer), file)) {
		buffer[strcspn(buffer, "\n")] = 0; /* remove trailing newline */

		if (strncmp(buffer, config_values[0], config_values_sizes[0]) ==
			0) {
			set->server = malloc(
				strlen(buffer + config_values_sizes[0]) + 1);
			strcpy(set->server, buffer + config_values_sizes[0]);
		} else if (strncmp(buffer,
				   config_values[1],
				   config_values_sizes[1]) == 0) {
			set->username = malloc(
				strlen(buffer + config_values_sizes[1]) + 1);
			strcpy(set->username, buffer + config_values_sizes[1]);
		} else if (strncmp(buffer,
				   config_values[2],
				   config_values_sizes[2]) == 0) {
			set->password = malloc(
				strlen(buffer + config_values_sizes[2]) + 1);
			strcpy(set->password, buffer + config_values_sizes[2]);
		}
	}

	fclose(file);
	return 0;
}

void destroy_config(struct settings *set)
{
	free(set->server);
	free(set->username);
	free(set->password);
}
